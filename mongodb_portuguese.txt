>> curso de mongodb
    podemos definir as tabelas no codigo
    schema ou modelo é o base da arquitetura da base de dados

    npm install --save express mongoose body-parser

    book.model.js
    'use strict';

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var BookSchema = new Schema({
        title   :   string,
        body    :   {
                        type        :   string,
                        required    :   true,
                        unique      :   true 
                    },
        published:  {
                        type        :   Date,
                        default     :   Date.now()
                    },
        keywords:   Array,
        published:  Boolean,
        author  :   {
                        //vai buscar o tipo a outro schema, tipo chave estrangeira, neste caso, 
                        // a referencia esta no modelo do User
                        type        :   Schema.ObjectId,
                        ref         :   'User'
                    },
        //embedded sub document ou sub arrays -> importante
        // para chegar aqui, seria: data.detail.modelNumber
        detail  :   {
                        modelNumber :   Number,
                        hardCover   :   Boolean,
                        reviews     :   Number,
                        rank        :   Number
                    }
    })

    module.exports = mongoose.model('Book', BookSchema);

>> retriving data from model

    https://mongoosejs.com/docs/queries.html

    comandos mongo:
        >> use example //mudar para db example
        >> show collections //mostrar tabelas
        
        >> comando: db.books.insert({title: '1983', author: 'Owrwell', category: 'Philosophy'})

        //inserir valores
        db.books.insert({
            title: 'Learn X',
            author: ' Susie X',
            Category: 'Humankind X'
        })
        db.books.insert({
            title: 'Learn X',
            author: ' Susie X',
            Category: 'Humankind X'
        })
        db.books.insert({
            title: 'Learn X',
            author: ' Susie X',
            Category: 'Humankind X'
        })

        >> db.books.find().pretty()
